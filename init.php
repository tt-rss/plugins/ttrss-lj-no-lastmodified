<?php
class LJ_No_LastModified extends Plugin {
	private $host;

	function about() {
		return array(1.0,
			"Force unconditional HTTP requests for Livejournal (also disables cache)",
			"fox");
	}

	function init($host) {
		$this->host = $host;

		$host->add_hook($host::HOOK_FETCH_FEED, $this);
	}

	function hook_fetch_feed($feed_data, $fetch_url, $owner_uid, $feed, $idk, $auth_login, $auth_pass) {
		if (mb_strpos($fetch_url, "livejournal.com/data") !== FALSE) {

			$feed_data = fetch_file_contents([
				"url" => $fetch_url,
				"login" => $auth_login,
				"pass" => $auth_pass,
				"last_modified" => ""
			]);

			return $feed_data;
		}
	}

	function api_version() {
		return 2;
	}

}
